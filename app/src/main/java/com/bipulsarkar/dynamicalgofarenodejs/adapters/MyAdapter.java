package com.bipulsarkar.dynamicalgofarenodejs.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bipulsarkar.dynamicalgofarenodejs.models.PersonalInfo;
import com.bipulsarkar.dynamicalgofarenodejs.R;
import com.bipulsarkar.dynamicalgofarenodejs.activitys.Show_Information;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    ArrayList<PersonalInfo> personalnfos=new ArrayList<>();
    Context context;

    public MyAdapter(ArrayList<PersonalInfo> personalnfos, Context context) {
        this.personalnfos = personalnfos;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.information_item, viewGroup, false);
        MyViewHolder v = new MyViewHolder(view);
        return v;

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final PersonalInfo p=personalnfos.get(i);
        myViewHolder.name.setText(p.getName());
        myViewHolder.email.setText(p.getEmail());
        //myViewHolder.citizen_rate.setText(p);
        myViewHolder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Show_Information.class);
                intent.putExtra("PersonName",p.getName());
                intent.putExtra("PersonMail",p.getEmail());
                intent.putExtra("PersonAbout",p.getAbout());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return personalnfos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,email,citizen_rate;
        CardView card;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.nameTV);
            email=itemView.findViewById(R.id.emailTV);
            card=itemView.findViewById(R.id.cardview);
            citizen_rate=itemView.findViewById(R.id.citizen_rate);
        }
    }
}
