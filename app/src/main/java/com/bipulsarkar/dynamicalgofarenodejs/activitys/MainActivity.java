package com.bipulsarkar.dynamicalgofarenodejs.activitys;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bipulsarkar.dynamicalgofarenodejs.R;

import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity {

    private FrameLayout buttonLayout_add,buttonLayout_search;
    private TextView text_add,text_search;
    private ProgressBar progressBarAdd,progressBarSearch;
    private View reveal;
    private int time = 2000;
    private long backpress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLayout_add = findViewById(R.id.buttonLayout_add);
        buttonLayout_search = findViewById(R.id.buttonLayout_search);
        text_add = findViewById(R.id.text_add);
        text_search = findViewById(R.id.text_search);
        progressBarAdd = findViewById(R.id.progress_bar_add);
        progressBarSearch = findViewById(R.id.progress_bar_search);
        reveal = findViewById(R.id.reveal);
    }

    public void load_add(View view) {
        animateButtonWidthAdd();
        fadeOutTextAndShowProgressBarForAdd();
        nextActionForAdd();
    }

    public void load_search(View view) {
        animateButtonWidthSearch();
        fadeOutTextAndShowProgressBarForSearch();
        nextActionForSearch();
    }


    private void animateButtonWidthAdd() {
        ValueAnimator anim = ValueAnimator.ofInt(buttonLayout_add.getMeasuredWidth(), getFinalWidth());

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = buttonLayout_add.getLayoutParams();
                layoutParams.width = val;
                buttonLayout_add.requestLayout();
            }
        });

        anim.setDuration(250);
        anim.start();
    }

    @Override
    public void onBackPressed() {
        if (backpress + time > System.currentTimeMillis()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(MainActivity.this,"Press again to exit",Toast.LENGTH_SHORT).show();
        }
        backpress = System.currentTimeMillis();

    }

    private void animateButtonWidthSearch() {
        ValueAnimator anim = ValueAnimator.ofInt(buttonLayout_search.getMeasuredWidth(), getFinalWidth());

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = buttonLayout_search.getLayoutParams();
                layoutParams.width = val;
                buttonLayout_search.requestLayout();
            }
        });

        anim.setDuration(250);
        anim.start();
    }

    private int getFinalWidth() {
        return (int) getResources().getDimension(R.dimen.fabSize);
    }


    private void fadeOutTextAndShowProgressBarForAdd() {
        text_add.animate()
                .alpha(0f)
                .setDuration(250)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        showProgressDialogAdd();
                    }
                })
                .start();
    }

    private void fadeOutTextAndShowProgressBarForSearch() {
        text_search.animate()
                .alpha(0f)
                .setDuration(250)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        showProgressDialogSearch();
                    }
                })
                .start();
    }

    private void showProgressDialogAdd() {
        progressBarAdd.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        progressBarAdd.setVisibility(VISIBLE);
    }

    private void showProgressDialogSearch() {
        progressBarSearch.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        progressBarSearch.setVisibility(VISIBLE);
    }


    private void nextActionForAdd() {
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                buttonLayout_search.setVisibility(View.GONE);
                revealButtonForAdd();
                fadeOutProgressBarAdd();
                startNextActivityForAdd();

            }
        }, 1000);
    }

    private void nextActionForSearch() {
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                buttonLayout_add.setVisibility(View.GONE);
                revealButtonForSearch();
                fadeOutProgressBarSearch();
                startNextActivityForSearch();

            }
        }, 1000);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    private void revealButtonForAdd() {
        buttonLayout_add.setElevation(0f);

        reveal.setVisibility(VISIBLE);

        int cx = reveal.getWidth();
        int cy = reveal.getHeight();

        int x =  (reveal.getLeft() + reveal.getRight())/2;
        int y = (reveal.getTop() + reveal.getBottom())/2;

        int startX = (int) (getFabWidth() / 2 + buttonLayout_add.getX());
        int startY = (int) (getFabWidth() / 2 + buttonLayout_add.getY());

        float finalRadius = Math.max(cx, cy) * 1.2f;

        Animator reveal = ViewAnimationUtils
                .createCircularReveal(findViewById(R.id.reveal), x, y, getFabWidth(), finalRadius);

        reveal.setDuration(500);
        reveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                finish();
            }
        });

        reveal.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealButtonForSearch() {
        buttonLayout_search.setElevation(0f);

        reveal.setVisibility(VISIBLE);

        int cx = reveal.getWidth();
        int cy = reveal.getHeight();

        int x =  (reveal.getLeft() + reveal.getRight())/2;
        int y = (reveal.getTop() + reveal.getBottom())/2;

        int startX = (int) (getFabWidth() / 2 + buttonLayout_search.getX());
        int startY = (int) (getFabWidth() / 2 + buttonLayout_search.getY());

        float finalRadius = Math.max(cx, cy) * 1.2f;

        Animator reveal = ViewAnimationUtils
                .createCircularReveal(findViewById(R.id.reveal), x, y, getFabWidth(), finalRadius);

        reveal.setDuration(500);
        reveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                finish();
            }
        });

        reveal.start();
    }

    private int getFabWidth() {
        return (int) getResources().getDimension(R.dimen.fabSize);
    }

    private void fadeOutProgressBarAdd() {
        progressBarAdd.animate().alpha(0f).setDuration(100).start();
    }
    private void fadeOutProgressBarSearch() {
        progressBarSearch.animate().alpha(0f).setDuration(200).start();
    }

    private void startNextActivityForAdd() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(
                        MainActivity.this, SubmitActivity.class));

                overridePendingTransition(R.anim.stay,R.anim.stay);
            }
        }, 100);
    }

    private void startNextActivityForSearch() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                startActivity(new Intent(
                        MainActivity.this, SearchNIDActivity.class));
                overridePendingTransition(R.anim.stay,R.anim.stay);
            }
        }, 100);
    }
}
