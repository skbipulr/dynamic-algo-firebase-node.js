package com.bipulsarkar.dynamicalgofarenodejs.activitys;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bipulsarkar.dynamicalgofarenodejs.R;
import com.bipulsarkar.dynamicalgofarenodejs.models.ProfileRequest;
import com.bipulsarkar.dynamicalgofarenodejs.models.ProfileResponse;
import com.bipulsarkar.dynamicalgofarenodejs.webApi.ProfileApi;
import com.bipulsarkar.dynamicalgofarenodejs.webApi.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubmitActivity extends AppCompatActivity {

    private Button saveBtn;
    private TextInputEditText nameET, emailET, aboutET,nidET;
    ProgressBar progressBar;

    private ProfileApi profileApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);

        //init filed

        saveBtn = findViewById(R.id.btnSubmit);
        nameET = findViewById(R.id.nameEdiT);
        emailET = findViewById(R.id.emailEdiT);
        aboutET = findViewById(R.id.aboutET);
        nidET = findViewById(R.id.nid_ET);

        profileApi = RetrofitClient.getClient().create(ProfileApi.class);


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPost();
            }
        });

    }

    private void createPost() {
        String name = nameET.getText().toString();
        String email = emailET.getText().toString();
        String aboutMe = aboutET.getText().toString();
        String nid = nidET.getText().toString();


        ProfileRequest profileRequest = new ProfileRequest();
        profileRequest.setName(name);
        profileRequest.setEmail(email);
        profileRequest.setAbout(aboutMe);
        profileRequest.setNID(nid);


        if(name.isEmpty()){
            nameET.setError("Please Enter Your Name.");
            nameET.requestFocus();
            return;
        }
        else if (email.isEmpty()){
            emailET.setError(Html.fromHtml("<font color='blue'>Please Enter Your Email.</font>"));
            emailET.requestFocus();
            return;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailET.setError("Please Enter Your Valid Email.");
            emailET.requestFocus();
            return;
        }

        else if (nid.isEmpty() ){
            nidET.setError("Please Enter Your valid NID.");
            nidET.requestFocus();
            return;
        }

        else if (aboutMe.isEmpty()){
            aboutET.setError("Please Enter Your Description.");
            aboutET.requestFocus();
            return;
        }
        else {
            Call<ProfileResponse> profileResponseCall = profileApi.getProfileInfo(profileRequest);
            profileResponseCall.enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                    //ProfileResponse profileResponse = response.body();
                    nameET.setText(null);
                    emailET.setText(null);
                    aboutET.setText(null);
                    nidET.setText(null);

                    if (response.code()==201){
                        Toast.makeText(SubmitActivity.this, "Congratulations!! Your info saved successfully.", Toast.LENGTH_SHORT).show();
                    }
                    else if (response.code() == 409){
                        Toast.makeText(SubmitActivity.this, "Sorry !! Your Email Already Exist!!", Toast.LENGTH_SHORT).show();
                    }

                    else {
                        Toast.makeText(SubmitActivity.this, "We are Sorry Your info is not saved !!", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ProfileResponse> call, Throwable t) {
                    Toast.makeText(SubmitActivity.this, "Error: "+t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

    }


    public void backPressed(View view) {
        startActivity(new Intent(SubmitActivity.this,MainActivity.class));
      //  finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
    }
}
