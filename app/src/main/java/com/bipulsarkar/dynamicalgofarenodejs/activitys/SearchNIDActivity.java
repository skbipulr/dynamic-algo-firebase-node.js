package com.bipulsarkar.dynamicalgofarenodejs.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bipulsarkar.dynamicalgofarenodejs.R;
import com.bipulsarkar.dynamicalgofarenodejs.models.NidRequest;
import com.bipulsarkar.dynamicalgofarenodejs.webApi.ProfileApi;
import com.bipulsarkar.dynamicalgofarenodejs.webApi.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchNIDActivity extends AppCompatActivity {

    private EditText searchET;
    private NidRequest users=new NidRequest();

    private TextView name,email,citizen_rate;

    private LinearLayout linearLayout;

    private View view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_nid);

        searchET = findViewById(R.id.sv_nid);
       // searchET.setSelection(3);

        name = findViewById(R.id.nameShow);
        email = findViewById(R.id.emailShow);
        citizen_rate = findViewById(R.id.citizen_rateShow);
        linearLayout = findViewById(R.id.linearLayout_ID);

     //   view  = findViewById(R.id.view_Id);

        fetchProfile("");

        findViewById(R.id.search_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = searchET.getText().toString();
              fetchProfile(input);
            }
        });

    }

    private void fetchProfile(final String keyword) {
        ProfileApi profileApi = RetrofitClient.getClientNID().create(ProfileApi.class);
        final Call<NidRequest> userCall = profileApi.getContactNid(keyword);
        userCall.enqueue(new Callback<NidRequest>() {
            @Override
            public void onResponse(Call<NidRequest> call, Response<NidRequest> response) {
                if (response.isSuccessful()) {
                    users = response.body();

               /*     Toast.makeText(SearchNIDActivity.this, "Got Response"+users.getUsername(), Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(SearchNIDActivity.this, Show_Information.class);
                    intent.putExtra("name",users.getUsername());
                    intent.putExtra("email",users.getEmail());
                    intent.putExtra("about",users.getAbout());
                    startActivity(intent);*/

               name.setText(users.getUsername());
               email.setText(users.getEmail());

               linearLayout.setVisibility(View.VISIBLE);
             //  view.setVisibility(View.VISIBLE);
                }
                else {
                    Toast.makeText(SearchNIDActivity.this, "Sorry: "+response.code(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<NidRequest> call, Throwable message) {
                Toast.makeText(SearchNIDActivity.this, "Error: " + message.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void backPressed(View view) {
        startActivity(new Intent(SearchNIDActivity.this,MainActivity.class));
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
    }
}
