package com.bipulsarkar.dynamicalgofarenodejs.webApi;

import com.bipulsarkar.dynamicalgofarenodejs.models.NidRequest;
import com.bipulsarkar.dynamicalgofarenodejs.models.ProfileRequest;
import com.bipulsarkar.dynamicalgofarenodejs.models.ProfileResponse;
import com.bipulsarkar.dynamicalgofarenodejs.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ProfileApi {

   // for firebase node.js api
    @POST("api/CreateUser")
    Call<ProfileResponse> getProfileInfo(@Body ProfileRequest profileRequest);

    @POST("api/GetList?Keyword=%s")
    Call<List<User>> getContact(@Query("Keyword") String keyword);

    @POST("api/GetUserInfo?NID=%s")
    Call<NidRequest> getContactNid(@Query("NID") String key);
}
