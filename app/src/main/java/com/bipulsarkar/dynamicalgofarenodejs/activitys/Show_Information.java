package com.bipulsarkar.dynamicalgofarenodejs.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.bipulsarkar.dynamicalgofarenodejs.R;

public class Show_Information extends AppCompatActivity {
    String Name;
    String About;
    String Email;
    TextView name,about,email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show__information);


        TextView nameTV = findViewById(R.id.Name);
        TextView emailTV = findViewById(R.id.Email);
        TextView aboutTV = findViewById(R.id.about);

        String name = getIntent().getStringExtra("name");
        String email = getIntent().getStringExtra("email");
        String about = getIntent().getStringExtra("about");

        nameTV.setText(name);
        emailTV.setText(email);
        aboutTV.setText(about);
    }

    public void backPressed(View view) {
        startActivity(new Intent(Show_Information.this, SearchNIDActivity.class));
        finish();
    }
}
