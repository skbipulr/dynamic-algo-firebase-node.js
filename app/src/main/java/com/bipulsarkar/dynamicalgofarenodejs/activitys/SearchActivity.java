package com.bipulsarkar.dynamicalgofarenodejs.activitys;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bipulsarkar.dynamicalgofarenodejs.R;
import com.bipulsarkar.dynamicalgofarenodejs.adapters.SearchAdapter;
import com.bipulsarkar.dynamicalgofarenodejs.models.User;
import com.bipulsarkar.dynamicalgofarenodejs.webApi.ProfileApi;
import com.bipulsarkar.dynamicalgofarenodejs.webApi.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private SearchAdapter adapter;
    private ProgressBar progressBar;

    private List<User> userInfo = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = findViewById(R.id.RecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        progressBar = findViewById(R.id.progress_bar_search);
        fetchProfile("");
    }


    private void fetchProfile(final String keyword) {
        ProfileApi profileApi = RetrofitClient.getClient().create(ProfileApi.class);
        final Call<List<User>> userCall = profileApi.getContact(keyword);
        userCall.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    userInfo = response.body();
                    //  Toast.makeText(SearchActivity.this, ""+userInfo.get(0).getUsername(), Toast.LENGTH_SHORT).show();
                    adapter = new SearchAdapter(userInfo, SearchActivity.this);
                    recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable message) {
                Toast.makeText(SearchActivity.this, "Error: " + message.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String keyword) {
                fetchProfile(keyword);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String query) {
                fetchProfile(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SearchActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(this, MainActivity.class));
        return true;
    }
}

