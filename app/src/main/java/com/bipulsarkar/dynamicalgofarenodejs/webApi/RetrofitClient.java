package com.bipulsarkar.dynamicalgofarenodejs.webApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    public static Retrofit getClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://dynamicalgonode.firebaseapp.com/")
                //  .baseUrl("http://www.dynamicalgo.somee.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Retrofit getClientNID(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://dynamicalgonode.firebaseapp.com/")
                //  .baseUrl("http://www.dynamicalgo.somee.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }
}
