package com.bipulsarkar.dynamicalgofarenodejs.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bipulsarkar.dynamicalgofarenodejs.R;
import com.bipulsarkar.dynamicalgofarenodejs.activitys.Show_Information;
import com.bipulsarkar.dynamicalgofarenodejs.models.User;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

    private List<User> users;
    private Context context;

    public SearchAdapter(List<User> users, Context context) {
        this.users = users;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.information_item,  parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final User getItemId = users.get(position);
        holder.nameTV.setText(getItemId.getUsername());
        holder.emailTV.setText(getItemId.getEmail());

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV, emailTV,nidTV;
        CardView card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.nameTV);
            emailTV = itemView.findViewById(R.id.emailTV);
            nidTV = itemView.findViewById(R.id.citizen_rate);

            card = itemView.findViewById(R.id.cardview);
        }
    }
}
